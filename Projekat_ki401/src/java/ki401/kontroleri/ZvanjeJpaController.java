/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.kontroleri;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ki401.entiteti.Zaposlen;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import ki401.entiteti.Zvanje;
import ki401.kontroleri.exceptions.IllegalOrphanException;
import ki401.kontroleri.exceptions.NonexistentEntityException;
import ki401.kontroleri.exceptions.RollbackFailureException;

/**
 *
 * @author taklap
 */
public class ZvanjeJpaController implements Serializable {

    public ZvanjeJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Zvanje zvanje) throws RollbackFailureException, Exception {
        if (zvanje.getZaposlenCollection() == null) {
            zvanje.setZaposlenCollection(new ArrayList<Zaposlen>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Zaposlen> attachedZaposlenCollection = new ArrayList<Zaposlen>();
            for (Zaposlen zaposlenCollectionZaposlenToAttach : zvanje.getZaposlenCollection()) {
                zaposlenCollectionZaposlenToAttach = em.getReference(zaposlenCollectionZaposlenToAttach.getClass(), zaposlenCollectionZaposlenToAttach.getKorisnikId());
                attachedZaposlenCollection.add(zaposlenCollectionZaposlenToAttach);
            }
            zvanje.setZaposlenCollection(attachedZaposlenCollection);
            em.persist(zvanje);
            for (Zaposlen zaposlenCollectionZaposlen : zvanje.getZaposlenCollection()) {
                Zvanje oldZvanjeIdOfZaposlenCollectionZaposlen = zaposlenCollectionZaposlen.getZvanjeId();
                zaposlenCollectionZaposlen.setZvanjeId(zvanje);
                zaposlenCollectionZaposlen = em.merge(zaposlenCollectionZaposlen);
                if (oldZvanjeIdOfZaposlenCollectionZaposlen != null) {
                    oldZvanjeIdOfZaposlenCollectionZaposlen.getZaposlenCollection().remove(zaposlenCollectionZaposlen);
                    oldZvanjeIdOfZaposlenCollectionZaposlen = em.merge(oldZvanjeIdOfZaposlenCollectionZaposlen);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Zvanje zvanje) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Zvanje persistentZvanje = em.find(Zvanje.class, zvanje.getZvanjeId());
            Collection<Zaposlen> zaposlenCollectionOld = persistentZvanje.getZaposlenCollection();
            Collection<Zaposlen> zaposlenCollectionNew = zvanje.getZaposlenCollection();
            List<String> illegalOrphanMessages = null;
            for (Zaposlen zaposlenCollectionOldZaposlen : zaposlenCollectionOld) {
                if (!zaposlenCollectionNew.contains(zaposlenCollectionOldZaposlen)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Zaposlen " + zaposlenCollectionOldZaposlen + " since its zvanjeId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Zaposlen> attachedZaposlenCollectionNew = new ArrayList<Zaposlen>();
            for (Zaposlen zaposlenCollectionNewZaposlenToAttach : zaposlenCollectionNew) {
                zaposlenCollectionNewZaposlenToAttach = em.getReference(zaposlenCollectionNewZaposlenToAttach.getClass(), zaposlenCollectionNewZaposlenToAttach.getKorisnikId());
                attachedZaposlenCollectionNew.add(zaposlenCollectionNewZaposlenToAttach);
            }
            zaposlenCollectionNew = attachedZaposlenCollectionNew;
            zvanje.setZaposlenCollection(zaposlenCollectionNew);
            zvanje = em.merge(zvanje);
            for (Zaposlen zaposlenCollectionNewZaposlen : zaposlenCollectionNew) {
                if (!zaposlenCollectionOld.contains(zaposlenCollectionNewZaposlen)) {
                    Zvanje oldZvanjeIdOfZaposlenCollectionNewZaposlen = zaposlenCollectionNewZaposlen.getZvanjeId();
                    zaposlenCollectionNewZaposlen.setZvanjeId(zvanje);
                    zaposlenCollectionNewZaposlen = em.merge(zaposlenCollectionNewZaposlen);
                    if (oldZvanjeIdOfZaposlenCollectionNewZaposlen != null && !oldZvanjeIdOfZaposlenCollectionNewZaposlen.equals(zvanje)) {
                        oldZvanjeIdOfZaposlenCollectionNewZaposlen.getZaposlenCollection().remove(zaposlenCollectionNewZaposlen);
                        oldZvanjeIdOfZaposlenCollectionNewZaposlen = em.merge(oldZvanjeIdOfZaposlenCollectionNewZaposlen);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = zvanje.getZvanjeId();
                if (findZvanje(id) == null) {
                    throw new NonexistentEntityException("The zvanje with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Zvanje zvanje;
            try {
                zvanje = em.getReference(Zvanje.class, id);
                zvanje.getZvanjeId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The zvanje with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Zaposlen> zaposlenCollectionOrphanCheck = zvanje.getZaposlenCollection();
            for (Zaposlen zaposlenCollectionOrphanCheckZaposlen : zaposlenCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Zvanje (" + zvanje + ") cannot be destroyed since the Zaposlen " + zaposlenCollectionOrphanCheckZaposlen + " in its zaposlenCollection field has a non-nullable zvanjeId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(zvanje);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Zvanje> findZvanjeEntities() {
        return findZvanjeEntities(true, -1, -1);
    }

    public List<Zvanje> findZvanjeEntities(int maxResults, int firstResult) {
        return findZvanjeEntities(false, maxResults, firstResult);
    }

    private List<Zvanje> findZvanjeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Zvanje.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Zvanje findZvanje(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Zvanje.class, id);
        } finally {
            em.close();
        }
    }

    public int getZvanjeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Zvanje> rt = cq.from(Zvanje.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
