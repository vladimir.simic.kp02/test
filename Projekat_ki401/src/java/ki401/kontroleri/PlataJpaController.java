/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.kontroleri;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import ki401.entiteti.Plata;
import ki401.entiteti.Zaposlen;
import ki401.kontroleri.exceptions.NonexistentEntityException;
import ki401.kontroleri.exceptions.RollbackFailureException;

/**
 *
 * @author taklap
 */
public class PlataJpaController implements Serializable {

    public PlataJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Plata plata) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Zaposlen korisnikId = plata.getKorisnikId();
            if (korisnikId != null) {
                korisnikId = em.getReference(korisnikId.getClass(), korisnikId.getKorisnikId());
                plata.setKorisnikId(korisnikId);
            }
            em.persist(plata);
            if (korisnikId != null) {
                korisnikId.getPlataCollection().add(plata);
                korisnikId = em.merge(korisnikId);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Plata plata) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Plata persistentPlata = em.find(Plata.class, plata.getPlataId());
            Zaposlen korisnikIdOld = persistentPlata.getKorisnikId();
            Zaposlen korisnikIdNew = plata.getKorisnikId();
            if (korisnikIdNew != null) {
                korisnikIdNew = em.getReference(korisnikIdNew.getClass(), korisnikIdNew.getKorisnikId());
                plata.setKorisnikId(korisnikIdNew);
            }
            plata = em.merge(plata);
            if (korisnikIdOld != null && !korisnikIdOld.equals(korisnikIdNew)) {
                korisnikIdOld.getPlataCollection().remove(plata);
                korisnikIdOld = em.merge(korisnikIdOld);
            }
            if (korisnikIdNew != null && !korisnikIdNew.equals(korisnikIdOld)) {
                korisnikIdNew.getPlataCollection().add(plata);
                korisnikIdNew = em.merge(korisnikIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = plata.getPlataId();
                if (findPlata(id) == null) {
                    throw new NonexistentEntityException("The plata with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Plata plata;
            try {
                plata = em.getReference(Plata.class, id);
                plata.getPlataId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The plata with id " + id + " no longer exists.", enfe);
            }
            Zaposlen korisnikId = plata.getKorisnikId();
            if (korisnikId != null) {
                korisnikId.getPlataCollection().remove(plata);
                korisnikId = em.merge(korisnikId);
            }
            em.remove(plata);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Plata> findPlataEntities() {
        return findPlataEntities(true, -1, -1);
    }

    public List<Plata> findPlataEntities(int maxResults, int firstResult) {
        return findPlataEntities(false, maxResults, firstResult);
    }

    private List<Plata> findPlataEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Plata.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Plata findPlata(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Plata.class, id);
        } finally {
            em.close();
        }
    }

    public int getPlataCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Plata> rt = cq.from(Plata.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
