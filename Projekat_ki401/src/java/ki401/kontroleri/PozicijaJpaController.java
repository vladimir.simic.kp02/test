/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.kontroleri;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ki401.entiteti.Zaposlen;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import ki401.entiteti.Pozicija;
import ki401.kontroleri.exceptions.IllegalOrphanException;
import ki401.kontroleri.exceptions.NonexistentEntityException;
import ki401.kontroleri.exceptions.RollbackFailureException;

/**
 *
 * @author taklap
 */
public class PozicijaJpaController implements Serializable {

    public PozicijaJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pozicija pozicija) throws RollbackFailureException, Exception {
        if (pozicija.getZaposlenCollection() == null) {
            pozicija.setZaposlenCollection(new ArrayList<Zaposlen>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Zaposlen> attachedZaposlenCollection = new ArrayList<Zaposlen>();
            for (Zaposlen zaposlenCollectionZaposlenToAttach : pozicija.getZaposlenCollection()) {
                zaposlenCollectionZaposlenToAttach = em.getReference(zaposlenCollectionZaposlenToAttach.getClass(), zaposlenCollectionZaposlenToAttach.getKorisnikId());
                attachedZaposlenCollection.add(zaposlenCollectionZaposlenToAttach);
            }
            pozicija.setZaposlenCollection(attachedZaposlenCollection);
            em.persist(pozicija);
            for (Zaposlen zaposlenCollectionZaposlen : pozicija.getZaposlenCollection()) {
                Pozicija oldPozicijaIdOfZaposlenCollectionZaposlen = zaposlenCollectionZaposlen.getPozicijaId();
                zaposlenCollectionZaposlen.setPozicijaId(pozicija);
                zaposlenCollectionZaposlen = em.merge(zaposlenCollectionZaposlen);
                if (oldPozicijaIdOfZaposlenCollectionZaposlen != null) {
                    oldPozicijaIdOfZaposlenCollectionZaposlen.getZaposlenCollection().remove(zaposlenCollectionZaposlen);
                    oldPozicijaIdOfZaposlenCollectionZaposlen = em.merge(oldPozicijaIdOfZaposlenCollectionZaposlen);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pozicija pozicija) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Pozicija persistentPozicija = em.find(Pozicija.class, pozicija.getPozicijaId());
            Collection<Zaposlen> zaposlenCollectionOld = persistentPozicija.getZaposlenCollection();
            Collection<Zaposlen> zaposlenCollectionNew = pozicija.getZaposlenCollection();
            List<String> illegalOrphanMessages = null;
            for (Zaposlen zaposlenCollectionOldZaposlen : zaposlenCollectionOld) {
                if (!zaposlenCollectionNew.contains(zaposlenCollectionOldZaposlen)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Zaposlen " + zaposlenCollectionOldZaposlen + " since its pozicijaId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Zaposlen> attachedZaposlenCollectionNew = new ArrayList<Zaposlen>();
            for (Zaposlen zaposlenCollectionNewZaposlenToAttach : zaposlenCollectionNew) {
                zaposlenCollectionNewZaposlenToAttach = em.getReference(zaposlenCollectionNewZaposlenToAttach.getClass(), zaposlenCollectionNewZaposlenToAttach.getKorisnikId());
                attachedZaposlenCollectionNew.add(zaposlenCollectionNewZaposlenToAttach);
            }
            zaposlenCollectionNew = attachedZaposlenCollectionNew;
            pozicija.setZaposlenCollection(zaposlenCollectionNew);
            pozicija = em.merge(pozicija);
            for (Zaposlen zaposlenCollectionNewZaposlen : zaposlenCollectionNew) {
                if (!zaposlenCollectionOld.contains(zaposlenCollectionNewZaposlen)) {
                    Pozicija oldPozicijaIdOfZaposlenCollectionNewZaposlen = zaposlenCollectionNewZaposlen.getPozicijaId();
                    zaposlenCollectionNewZaposlen.setPozicijaId(pozicija);
                    zaposlenCollectionNewZaposlen = em.merge(zaposlenCollectionNewZaposlen);
                    if (oldPozicijaIdOfZaposlenCollectionNewZaposlen != null && !oldPozicijaIdOfZaposlenCollectionNewZaposlen.equals(pozicija)) {
                        oldPozicijaIdOfZaposlenCollectionNewZaposlen.getZaposlenCollection().remove(zaposlenCollectionNewZaposlen);
                        oldPozicijaIdOfZaposlenCollectionNewZaposlen = em.merge(oldPozicijaIdOfZaposlenCollectionNewZaposlen);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pozicija.getPozicijaId();
                if (findPozicija(id) == null) {
                    throw new NonexistentEntityException("The pozicija with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Pozicija pozicija;
            try {
                pozicija = em.getReference(Pozicija.class, id);
                pozicija.getPozicijaId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pozicija with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Zaposlen> zaposlenCollectionOrphanCheck = pozicija.getZaposlenCollection();
            for (Zaposlen zaposlenCollectionOrphanCheckZaposlen : zaposlenCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pozicija (" + pozicija + ") cannot be destroyed since the Zaposlen " + zaposlenCollectionOrphanCheckZaposlen + " in its zaposlenCollection field has a non-nullable pozicijaId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(pozicija);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pozicija> findPozicijaEntities() {
        return findPozicijaEntities(true, -1, -1);
    }

    public List<Pozicija> findPozicijaEntities(int maxResults, int firstResult) {
        return findPozicijaEntities(false, maxResults, firstResult);
    }

    private List<Pozicija> findPozicijaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pozicija.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pozicija findPozicija(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pozicija.class, id);
        } finally {
            em.close();
        }
    }

    public int getPozicijaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pozicija> rt = cq.from(Pozicija.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
