/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.kontroleri;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ki401.entiteti.Korisnik;
import ki401.entiteti.Pozicija;
import ki401.entiteti.Zvanje;
import ki401.entiteti.Predmet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import ki401.entiteti.Plata;
import ki401.entiteti.Zaposlen;
import ki401.kontroleri.exceptions.IllegalOrphanException;
import ki401.kontroleri.exceptions.NonexistentEntityException;
import ki401.kontroleri.exceptions.PreexistingEntityException;
import ki401.kontroleri.exceptions.RollbackFailureException;

/**
 *
 * @author taklap
 */
public class ZaposlenJpaController implements Serializable {

    public ZaposlenJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Zaposlen zaposlen) throws IllegalOrphanException, PreexistingEntityException, RollbackFailureException, Exception {
        if (zaposlen.getPredmetCollection() == null) {
            zaposlen.setPredmetCollection(new ArrayList<Predmet>());
        }
        if (zaposlen.getPlataCollection() == null) {
            zaposlen.setPlataCollection(new ArrayList<Plata>());
        }
        List<String> illegalOrphanMessages = null;
        Korisnik korisnikOrphanCheck = zaposlen.getKorisnik();
        if (korisnikOrphanCheck != null) {
            Zaposlen oldZaposlenOfKorisnik = korisnikOrphanCheck.getZaposlen();
            if (oldZaposlenOfKorisnik != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The Korisnik " + korisnikOrphanCheck + " already has an item of type Zaposlen whose korisnik column cannot be null. Please make another selection for the korisnik field.");
            }
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Korisnik korisnik = zaposlen.getKorisnik();
            if (korisnik != null) {
                korisnik = em.getReference(korisnik.getClass(), korisnik.getKorisnikId());
                zaposlen.setKorisnik(korisnik);
            }
            Pozicija pozicijaId = zaposlen.getPozicijaId();
            if (pozicijaId != null) {
                pozicijaId = em.getReference(pozicijaId.getClass(), pozicijaId.getPozicijaId());
                zaposlen.setPozicijaId(pozicijaId);
            }
            Zvanje zvanjeId = zaposlen.getZvanjeId();
            if (zvanjeId != null) {
                zvanjeId = em.getReference(zvanjeId.getClass(), zvanjeId.getZvanjeId());
                zaposlen.setZvanjeId(zvanjeId);
            }
            Collection<Predmet> attachedPredmetCollection = new ArrayList<Predmet>();
            for (Predmet predmetCollectionPredmetToAttach : zaposlen.getPredmetCollection()) {
                predmetCollectionPredmetToAttach = em.getReference(predmetCollectionPredmetToAttach.getClass(), predmetCollectionPredmetToAttach.getPredmetId());
                attachedPredmetCollection.add(predmetCollectionPredmetToAttach);
            }
            zaposlen.setPredmetCollection(attachedPredmetCollection);
            Collection<Plata> attachedPlataCollection = new ArrayList<Plata>();
            for (Plata plataCollectionPlataToAttach : zaposlen.getPlataCollection()) {
                plataCollectionPlataToAttach = em.getReference(plataCollectionPlataToAttach.getClass(), plataCollectionPlataToAttach.getPlataId());
                attachedPlataCollection.add(plataCollectionPlataToAttach);
            }
            zaposlen.setPlataCollection(attachedPlataCollection);
            em.persist(zaposlen);
            if (korisnik != null) {
                korisnik.setZaposlen(zaposlen);
                korisnik = em.merge(korisnik);
            }
            if (pozicijaId != null) {
                pozicijaId.getZaposlenCollection().add(zaposlen);
                pozicijaId = em.merge(pozicijaId);
            }
            if (zvanjeId != null) {
                zvanjeId.getZaposlenCollection().add(zaposlen);
                zvanjeId = em.merge(zvanjeId);
            }
            for (Predmet predmetCollectionPredmet : zaposlen.getPredmetCollection()) {
                predmetCollectionPredmet.getZaposlenCollection().add(zaposlen);
                predmetCollectionPredmet = em.merge(predmetCollectionPredmet);
            }
            for (Plata plataCollectionPlata : zaposlen.getPlataCollection()) {
                Zaposlen oldKorisnikIdOfPlataCollectionPlata = plataCollectionPlata.getKorisnikId();
                plataCollectionPlata.setKorisnikId(zaposlen);
                plataCollectionPlata = em.merge(plataCollectionPlata);
                if (oldKorisnikIdOfPlataCollectionPlata != null) {
                    oldKorisnikIdOfPlataCollectionPlata.getPlataCollection().remove(plataCollectionPlata);
                    oldKorisnikIdOfPlataCollectionPlata = em.merge(oldKorisnikIdOfPlataCollectionPlata);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findZaposlen(zaposlen.getKorisnikId()) != null) {
                throw new PreexistingEntityException("Zaposlen " + zaposlen + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Zaposlen zaposlen) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Zaposlen persistentZaposlen = em.find(Zaposlen.class, zaposlen.getKorisnikId());
            Korisnik korisnikOld = persistentZaposlen.getKorisnik();
            Korisnik korisnikNew = zaposlen.getKorisnik();
            Pozicija pozicijaIdOld = persistentZaposlen.getPozicijaId();
            Pozicija pozicijaIdNew = zaposlen.getPozicijaId();
            Zvanje zvanjeIdOld = persistentZaposlen.getZvanjeId();
            Zvanje zvanjeIdNew = zaposlen.getZvanjeId();
            Collection<Predmet> predmetCollectionOld = persistentZaposlen.getPredmetCollection();
            Collection<Predmet> predmetCollectionNew = zaposlen.getPredmetCollection();
            Collection<Plata> plataCollectionOld = persistentZaposlen.getPlataCollection();
            Collection<Plata> plataCollectionNew = zaposlen.getPlataCollection();
            List<String> illegalOrphanMessages = null;
            if (korisnikNew != null && !korisnikNew.equals(korisnikOld)) {
                Zaposlen oldZaposlenOfKorisnik = korisnikNew.getZaposlen();
                if (oldZaposlenOfKorisnik != null) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The Korisnik " + korisnikNew + " already has an item of type Zaposlen whose korisnik column cannot be null. Please make another selection for the korisnik field.");
                }
            }
            for (Plata plataCollectionOldPlata : plataCollectionOld) {
                if (!plataCollectionNew.contains(plataCollectionOldPlata)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Plata " + plataCollectionOldPlata + " since its korisnikId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (korisnikNew != null) {
                korisnikNew = em.getReference(korisnikNew.getClass(), korisnikNew.getKorisnikId());
                zaposlen.setKorisnik(korisnikNew);
            }
            if (pozicijaIdNew != null) {
                pozicijaIdNew = em.getReference(pozicijaIdNew.getClass(), pozicijaIdNew.getPozicijaId());
                zaposlen.setPozicijaId(pozicijaIdNew);
            }
            if (zvanjeIdNew != null) {
                zvanjeIdNew = em.getReference(zvanjeIdNew.getClass(), zvanjeIdNew.getZvanjeId());
                zaposlen.setZvanjeId(zvanjeIdNew);
            }
            Collection<Predmet> attachedPredmetCollectionNew = new ArrayList<Predmet>();
            for (Predmet predmetCollectionNewPredmetToAttach : predmetCollectionNew) {
                predmetCollectionNewPredmetToAttach = em.getReference(predmetCollectionNewPredmetToAttach.getClass(), predmetCollectionNewPredmetToAttach.getPredmetId());
                attachedPredmetCollectionNew.add(predmetCollectionNewPredmetToAttach);
            }
            predmetCollectionNew = attachedPredmetCollectionNew;
            zaposlen.setPredmetCollection(predmetCollectionNew);
            Collection<Plata> attachedPlataCollectionNew = new ArrayList<Plata>();
            for (Plata plataCollectionNewPlataToAttach : plataCollectionNew) {
                plataCollectionNewPlataToAttach = em.getReference(plataCollectionNewPlataToAttach.getClass(), plataCollectionNewPlataToAttach.getPlataId());
                attachedPlataCollectionNew.add(plataCollectionNewPlataToAttach);
            }
            plataCollectionNew = attachedPlataCollectionNew;
            zaposlen.setPlataCollection(plataCollectionNew);
            zaposlen = em.merge(zaposlen);
            if (korisnikOld != null && !korisnikOld.equals(korisnikNew)) {
                korisnikOld.setZaposlen(null);
                korisnikOld = em.merge(korisnikOld);
            }
            if (korisnikNew != null && !korisnikNew.equals(korisnikOld)) {
                korisnikNew.setZaposlen(zaposlen);
                korisnikNew = em.merge(korisnikNew);
            }
            if (pozicijaIdOld != null && !pozicijaIdOld.equals(pozicijaIdNew)) {
                pozicijaIdOld.getZaposlenCollection().remove(zaposlen);
                pozicijaIdOld = em.merge(pozicijaIdOld);
            }
            if (pozicijaIdNew != null && !pozicijaIdNew.equals(pozicijaIdOld)) {
                pozicijaIdNew.getZaposlenCollection().add(zaposlen);
                pozicijaIdNew = em.merge(pozicijaIdNew);
            }
            if (zvanjeIdOld != null && !zvanjeIdOld.equals(zvanjeIdNew)) {
                zvanjeIdOld.getZaposlenCollection().remove(zaposlen);
                zvanjeIdOld = em.merge(zvanjeIdOld);
            }
            if (zvanjeIdNew != null && !zvanjeIdNew.equals(zvanjeIdOld)) {
                zvanjeIdNew.getZaposlenCollection().add(zaposlen);
                zvanjeIdNew = em.merge(zvanjeIdNew);
            }
            for (Predmet predmetCollectionOldPredmet : predmetCollectionOld) {
                if (!predmetCollectionNew.contains(predmetCollectionOldPredmet)) {
                    predmetCollectionOldPredmet.getZaposlenCollection().remove(zaposlen);
                    predmetCollectionOldPredmet = em.merge(predmetCollectionOldPredmet);
                }
            }
            for (Predmet predmetCollectionNewPredmet : predmetCollectionNew) {
                if (!predmetCollectionOld.contains(predmetCollectionNewPredmet)) {
                    predmetCollectionNewPredmet.getZaposlenCollection().add(zaposlen);
                    predmetCollectionNewPredmet = em.merge(predmetCollectionNewPredmet);
                }
            }
            for (Plata plataCollectionNewPlata : plataCollectionNew) {
                if (!plataCollectionOld.contains(plataCollectionNewPlata)) {
                    Zaposlen oldKorisnikIdOfPlataCollectionNewPlata = plataCollectionNewPlata.getKorisnikId();
                    plataCollectionNewPlata.setKorisnikId(zaposlen);
                    plataCollectionNewPlata = em.merge(plataCollectionNewPlata);
                    if (oldKorisnikIdOfPlataCollectionNewPlata != null && !oldKorisnikIdOfPlataCollectionNewPlata.equals(zaposlen)) {
                        oldKorisnikIdOfPlataCollectionNewPlata.getPlataCollection().remove(plataCollectionNewPlata);
                        oldKorisnikIdOfPlataCollectionNewPlata = em.merge(oldKorisnikIdOfPlataCollectionNewPlata);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = zaposlen.getKorisnikId();
                if (findZaposlen(id) == null) {
                    throw new NonexistentEntityException("The zaposlen with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Zaposlen zaposlen;
            try {
                zaposlen = em.getReference(Zaposlen.class, id);
                zaposlen.getKorisnikId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The zaposlen with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Plata> plataCollectionOrphanCheck = zaposlen.getPlataCollection();
            for (Plata plataCollectionOrphanCheckPlata : plataCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Zaposlen (" + zaposlen + ") cannot be destroyed since the Plata " + plataCollectionOrphanCheckPlata + " in its plataCollection field has a non-nullable korisnikId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Korisnik korisnik = zaposlen.getKorisnik();
            if (korisnik != null) {
                korisnik.setZaposlen(null);
                korisnik = em.merge(korisnik);
            }
            Pozicija pozicijaId = zaposlen.getPozicijaId();
            if (pozicijaId != null) {
                pozicijaId.getZaposlenCollection().remove(zaposlen);
                pozicijaId = em.merge(pozicijaId);
            }
            Zvanje zvanjeId = zaposlen.getZvanjeId();
            if (zvanjeId != null) {
                zvanjeId.getZaposlenCollection().remove(zaposlen);
                zvanjeId = em.merge(zvanjeId);
            }
            Collection<Predmet> predmetCollection = zaposlen.getPredmetCollection();
            for (Predmet predmetCollectionPredmet : predmetCollection) {
                predmetCollectionPredmet.getZaposlenCollection().remove(zaposlen);
                predmetCollectionPredmet = em.merge(predmetCollectionPredmet);
            }
            em.remove(zaposlen);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Zaposlen> findZaposlenEntities() {
        return findZaposlenEntities(true, -1, -1);
    }

    public List<Zaposlen> findZaposlenEntities(int maxResults, int firstResult) {
        return findZaposlenEntities(false, maxResults, firstResult);
    }

    private List<Zaposlen> findZaposlenEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Zaposlen.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Zaposlen findZaposlen(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Zaposlen.class, id);
        } finally {
            em.close();
        }
    }

    public int getZaposlenCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Zaposlen> rt = cq.from(Zaposlen.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
