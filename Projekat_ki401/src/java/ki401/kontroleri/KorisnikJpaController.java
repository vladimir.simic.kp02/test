/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.kontroleri;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ki401.entiteti.Zaposlen;
import ki401.entiteti.Rola;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import ki401.entiteti.Korisnik;
import ki401.kontroleri.exceptions.IllegalOrphanException;
import ki401.kontroleri.exceptions.NonexistentEntityException;
import ki401.kontroleri.exceptions.RollbackFailureException;

/**
 *
 * @author taklap
 */
public class KorisnikJpaController implements Serializable {

    public KorisnikJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Korisnik korisnik) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Zaposlen zaposlen = korisnik.getZaposlen();
            if (zaposlen != null) {
                zaposlen = em.getReference(zaposlen.getClass(), zaposlen.getKorisnikId());
                korisnik.setZaposlen(zaposlen);
            }
            Rola rolaId = korisnik.getRolaId();
            if (rolaId != null) {
                rolaId = em.getReference(rolaId.getClass(), rolaId.getRolaId());
                korisnik.setRolaId(rolaId);
            }
            em.persist(korisnik);
            if (zaposlen != null) {
                Korisnik oldKorisnikOfZaposlen = zaposlen.getKorisnik();
                if (oldKorisnikOfZaposlen != null) {
                    oldKorisnikOfZaposlen.setZaposlen(null);
                    oldKorisnikOfZaposlen = em.merge(oldKorisnikOfZaposlen);
                }
                zaposlen.setKorisnik(korisnik);
                zaposlen = em.merge(zaposlen);
            }
            if (rolaId != null) {
                rolaId.getKorisnikCollection().add(korisnik);
                rolaId = em.merge(rolaId);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Korisnik korisnik) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Korisnik persistentKorisnik = em.find(Korisnik.class, korisnik.getKorisnikId());
            Zaposlen zaposlenOld = persistentKorisnik.getZaposlen();
            Zaposlen zaposlenNew = korisnik.getZaposlen();
            Rola rolaIdOld = persistentKorisnik.getRolaId();
            Rola rolaIdNew = korisnik.getRolaId();
            List<String> illegalOrphanMessages = null;
            if (zaposlenOld != null && !zaposlenOld.equals(zaposlenNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain Zaposlen " + zaposlenOld + " since its korisnik field is not nullable.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (zaposlenNew != null) {
                zaposlenNew = em.getReference(zaposlenNew.getClass(), zaposlenNew.getKorisnikId());
                korisnik.setZaposlen(zaposlenNew);
            }
            if (rolaIdNew != null) {
                rolaIdNew = em.getReference(rolaIdNew.getClass(), rolaIdNew.getRolaId());
                korisnik.setRolaId(rolaIdNew);
            }
            korisnik = em.merge(korisnik);
            if (zaposlenNew != null && !zaposlenNew.equals(zaposlenOld)) {
                Korisnik oldKorisnikOfZaposlen = zaposlenNew.getKorisnik();
                if (oldKorisnikOfZaposlen != null) {
                    oldKorisnikOfZaposlen.setZaposlen(null);
                    oldKorisnikOfZaposlen = em.merge(oldKorisnikOfZaposlen);
                }
                zaposlenNew.setKorisnik(korisnik);
                zaposlenNew = em.merge(zaposlenNew);
            }
            if (rolaIdOld != null && !rolaIdOld.equals(rolaIdNew)) {
                rolaIdOld.getKorisnikCollection().remove(korisnik);
                rolaIdOld = em.merge(rolaIdOld);
            }
            if (rolaIdNew != null && !rolaIdNew.equals(rolaIdOld)) {
                rolaIdNew.getKorisnikCollection().add(korisnik);
                rolaIdNew = em.merge(rolaIdNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = korisnik.getKorisnikId();
                if (findKorisnik(id) == null) {
                    throw new NonexistentEntityException("The korisnik with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Korisnik korisnik;
            try {
                korisnik = em.getReference(Korisnik.class, id);
                korisnik.getKorisnikId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The korisnik with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Zaposlen zaposlenOrphanCheck = korisnik.getZaposlen();
            if (zaposlenOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Korisnik (" + korisnik + ") cannot be destroyed since the Zaposlen " + zaposlenOrphanCheck + " in its zaposlen field has a non-nullable korisnik field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Rola rolaId = korisnik.getRolaId();
            if (rolaId != null) {
                rolaId.getKorisnikCollection().remove(korisnik);
                rolaId = em.merge(rolaId);
            }
            em.remove(korisnik);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Korisnik> findKorisnikEntities() {
        return findKorisnikEntities(true, -1, -1);
    }

    public List<Korisnik> findKorisnikEntities(int maxResults, int firstResult) {
        return findKorisnikEntities(false, maxResults, firstResult);
    }

    private List<Korisnik> findKorisnikEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Korisnik.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Korisnik findKorisnik(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Korisnik.class, id);
        } finally {
            em.close();
        }
    }

    public int getKorisnikCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Korisnik> rt = cq.from(Korisnik.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
