/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.kontroleri;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ki401.entiteti.Zaposlen;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import ki401.entiteti.Predmet;
import ki401.kontroleri.exceptions.NonexistentEntityException;
import ki401.kontroleri.exceptions.RollbackFailureException;

/**
 *
 * @author taklap
 */
public class PredmetJpaController implements Serializable {

    public PredmetJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Predmet predmet) throws RollbackFailureException, Exception {
        if (predmet.getZaposlenCollection() == null) {
            predmet.setZaposlenCollection(new ArrayList<Zaposlen>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Zaposlen> attachedZaposlenCollection = new ArrayList<Zaposlen>();
            for (Zaposlen zaposlenCollectionZaposlenToAttach : predmet.getZaposlenCollection()) {
                zaposlenCollectionZaposlenToAttach = em.getReference(zaposlenCollectionZaposlenToAttach.getClass(), zaposlenCollectionZaposlenToAttach.getKorisnikId());
                attachedZaposlenCollection.add(zaposlenCollectionZaposlenToAttach);
            }
            predmet.setZaposlenCollection(attachedZaposlenCollection);
            em.persist(predmet);
            for (Zaposlen zaposlenCollectionZaposlen : predmet.getZaposlenCollection()) {
                zaposlenCollectionZaposlen.getPredmetCollection().add(predmet);
                zaposlenCollectionZaposlen = em.merge(zaposlenCollectionZaposlen);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Predmet predmet) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Predmet persistentPredmet = em.find(Predmet.class, predmet.getPredmetId());
            Collection<Zaposlen> zaposlenCollectionOld = persistentPredmet.getZaposlenCollection();
            Collection<Zaposlen> zaposlenCollectionNew = predmet.getZaposlenCollection();
            Collection<Zaposlen> attachedZaposlenCollectionNew = new ArrayList<Zaposlen>();
            for (Zaposlen zaposlenCollectionNewZaposlenToAttach : zaposlenCollectionNew) {
                zaposlenCollectionNewZaposlenToAttach = em.getReference(zaposlenCollectionNewZaposlenToAttach.getClass(), zaposlenCollectionNewZaposlenToAttach.getKorisnikId());
                attachedZaposlenCollectionNew.add(zaposlenCollectionNewZaposlenToAttach);
            }
            zaposlenCollectionNew = attachedZaposlenCollectionNew;
            predmet.setZaposlenCollection(zaposlenCollectionNew);
            predmet = em.merge(predmet);
            for (Zaposlen zaposlenCollectionOldZaposlen : zaposlenCollectionOld) {
                if (!zaposlenCollectionNew.contains(zaposlenCollectionOldZaposlen)) {
                    zaposlenCollectionOldZaposlen.getPredmetCollection().remove(predmet);
                    zaposlenCollectionOldZaposlen = em.merge(zaposlenCollectionOldZaposlen);
                }
            }
            for (Zaposlen zaposlenCollectionNewZaposlen : zaposlenCollectionNew) {
                if (!zaposlenCollectionOld.contains(zaposlenCollectionNewZaposlen)) {
                    zaposlenCollectionNewZaposlen.getPredmetCollection().add(predmet);
                    zaposlenCollectionNewZaposlen = em.merge(zaposlenCollectionNewZaposlen);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = predmet.getPredmetId();
                if (findPredmet(id) == null) {
                    throw new NonexistentEntityException("The predmet with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Predmet predmet;
            try {
                predmet = em.getReference(Predmet.class, id);
                predmet.getPredmetId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The predmet with id " + id + " no longer exists.", enfe);
            }
            Collection<Zaposlen> zaposlenCollection = predmet.getZaposlenCollection();
            for (Zaposlen zaposlenCollectionZaposlen : zaposlenCollection) {
                zaposlenCollectionZaposlen.getPredmetCollection().remove(predmet);
                zaposlenCollectionZaposlen = em.merge(zaposlenCollectionZaposlen);
            }
            em.remove(predmet);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Predmet> findPredmetEntities() {
        return findPredmetEntities(true, -1, -1);
    }

    public List<Predmet> findPredmetEntities(int maxResults, int firstResult) {
        return findPredmetEntities(false, maxResults, firstResult);
    }

    private List<Predmet> findPredmetEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Predmet.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Predmet findPredmet(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Predmet.class, id);
        } finally {
            em.close();
        }
    }

    public int getPredmetCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Predmet> rt = cq.from(Predmet.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
