/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.ws.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author taklap
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(ki401.ws.service.KorisnikFacadeREST.class);
        resources.add(ki401.ws.service.PlataFacadeREST.class);
        resources.add(ki401.ws.service.PozicijaFacadeREST.class);
        resources.add(ki401.ws.service.PredmetFacadeREST.class);
        resources.add(ki401.ws.service.RolaFacadeREST.class);
        resources.add(ki401.ws.service.ZaposlenFacadeREST.class);
        resources.add(ki401.ws.service.ZvanjeFacadeREST.class);
    }
    
}
