/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.entiteti;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author taklap
 */
@Entity
@Table(name = "predmeti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Predmet.findAll", query = "SELECT p FROM Predmet p")
    , @NamedQuery(name = "Predmet.findByPredmetId", query = "SELECT p FROM Predmet p WHERE p.predmetId = :predmetId")
    , @NamedQuery(name = "Predmet.findByPredmetSifra", query = "SELECT p FROM Predmet p WHERE p.predmetSifra = :predmetSifra")
    , @NamedQuery(name = "Predmet.findByPredmetNaziv", query = "SELECT p FROM Predmet p WHERE p.predmetNaziv = :predmetNaziv")})
public class Predmet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "predmet_id")
    private Integer predmetId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "predmet_sifra")
    private String predmetSifra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "predmet_naziv")
    private String predmetNaziv;
    @JoinTable(name = "zaposlenipredmeti", joinColumns = {
        @JoinColumn(name = "predmet_id", referencedColumnName = "predmet_id")}, inverseJoinColumns = {
        @JoinColumn(name = "korisnik_id", referencedColumnName = "korisnik_id")})
    @ManyToMany
    private Collection<Zaposlen> zaposlenCollection;

    public Predmet() {
    }

    public Predmet(Integer predmetId) {
        this.predmetId = predmetId;
    }

    public Predmet(Integer predmetId, String predmetSifra, String predmetNaziv) {
        this.predmetId = predmetId;
        this.predmetSifra = predmetSifra;
        this.predmetNaziv = predmetNaziv;
    }

    public Integer getPredmetId() {
        return predmetId;
    }

    public void setPredmetId(Integer predmetId) {
        this.predmetId = predmetId;
    }

    public String getPredmetSifra() {
        return predmetSifra;
    }

    public void setPredmetSifra(String predmetSifra) {
        this.predmetSifra = predmetSifra;
    }

    public String getPredmetNaziv() {
        return predmetNaziv;
    }

    public void setPredmetNaziv(String predmetNaziv) {
        this.predmetNaziv = predmetNaziv;
    }

    @XmlTransient
    public Collection<Zaposlen> getZaposlenCollection() {
        return zaposlenCollection;
    }

    public void setZaposlenCollection(Collection<Zaposlen> zaposlenCollection) {
        this.zaposlenCollection = zaposlenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (predmetId != null ? predmetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Predmet)) {
            return false;
        }
        Predmet other = (Predmet) object;
        if ((this.predmetId == null && other.predmetId != null) || (this.predmetId != null && !this.predmetId.equals(other.predmetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ki401.entiteti.Predmet[ predmetId=" + predmetId + " ]";
    }
    
}
