/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.entiteti;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author taklap
 */
@Entity
@Table(name = "zvanje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Zvanje.findAll", query = "SELECT z FROM Zvanje z")
    , @NamedQuery(name = "Zvanje.findByZvanjeId", query = "SELECT z FROM Zvanje z WHERE z.zvanjeId = :zvanjeId")
    , @NamedQuery(name = "Zvanje.findByZvanjeNaziv", query = "SELECT z FROM Zvanje z WHERE z.zvanjeNaziv = :zvanjeNaziv")})
public class Zvanje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "zvanje_id")
    private Integer zvanjeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "zvanje_naziv")
    private String zvanjeNaziv;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "zvanjeId")
    private Collection<Zaposlen> zaposlenCollection;

    public Zvanje() {
    }

    public Zvanje(Integer zvanjeId) {
        this.zvanjeId = zvanjeId;
    }

    public Zvanje(Integer zvanjeId, String zvanjeNaziv) {
        this.zvanjeId = zvanjeId;
        this.zvanjeNaziv = zvanjeNaziv;
    }

    public Integer getZvanjeId() {
        return zvanjeId;
    }

    public void setZvanjeId(Integer zvanjeId) {
        this.zvanjeId = zvanjeId;
    }

    public String getZvanjeNaziv() {
        return zvanjeNaziv;
    }

    public void setZvanjeNaziv(String zvanjeNaziv) {
        this.zvanjeNaziv = zvanjeNaziv;
    }

    @XmlTransient
    public Collection<Zaposlen> getZaposlenCollection() {
        return zaposlenCollection;
    }

    public void setZaposlenCollection(Collection<Zaposlen> zaposlenCollection) {
        this.zaposlenCollection = zaposlenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (zvanjeId != null ? zvanjeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zvanje)) {
            return false;
        }
        Zvanje other = (Zvanje) object;
        if ((this.zvanjeId == null && other.zvanjeId != null) || (this.zvanjeId != null && !this.zvanjeId.equals(other.zvanjeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ki401.entiteti.Zvanje[ zvanjeId=" + zvanjeId + " ]";
    }
    
}
