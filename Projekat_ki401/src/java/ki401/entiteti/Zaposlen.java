/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.entiteti;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author taklap
 */
@Entity
@Table(name = "zaposleni")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Zaposlen.findAll", query = "SELECT z FROM Zaposlen z")
    , @NamedQuery(name = "Zaposlen.findByKorisnikId", query = "SELECT z FROM Zaposlen z WHERE z.korisnikId = :korisnikId")
    , @NamedQuery(name = "Zaposlen.findByAngazovanOd", query = "SELECT z FROM Zaposlen z WHERE z.angazovanOd = :angazovanOd")
    , @NamedQuery(name = "Zaposlen.findByAngazovanDo", query = "SELECT z FROM Zaposlen z WHERE z.angazovanDo = :angazovanDo")
    , @NamedQuery(name = "Zaposlen.findByBrojRacuna", query = "SELECT z FROM Zaposlen z WHERE z.brojRacuna = :brojRacuna")})
public class Zaposlen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "korisnik_id")
    private Integer korisnikId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "angazovan_od")
    @Temporal(TemporalType.DATE)
    private Date angazovanOd;
    @Column(name = "angazovan_do")
    @Temporal(TemporalType.DATE)
    private Date angazovanDo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "broj_racuna")
    private String brojRacuna;
    @ManyToMany(mappedBy = "zaposlenCollection")
    private Collection<Predmet> predmetCollection;
    @JoinColumn(name = "korisnik_id", referencedColumnName = "korisnik_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Korisnik korisnik;
    @JoinColumn(name = "pozicija_id", referencedColumnName = "pozicija_id")
    @ManyToOne(optional = false)
    private Pozicija pozicijaId;
    @JoinColumn(name = "zvanje_id", referencedColumnName = "zvanje_id")
    @ManyToOne(optional = false)
    private Zvanje zvanjeId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "korisnikId")
    private Collection<Plata> plataCollection;

    public Zaposlen() {
    }

    public Zaposlen(Integer korisnikId) {
        this.korisnikId = korisnikId;
    }

    public Zaposlen(Integer korisnikId, Date angazovanOd, String brojRacuna) {
        this.korisnikId = korisnikId;
        this.angazovanOd = angazovanOd;
        this.brojRacuna = brojRacuna;
    }

    public Integer getKorisnikId() {
        return korisnikId;
    }

    public void setKorisnikId(Integer korisnikId) {
        this.korisnikId = korisnikId;
    }

    public Date getAngazovanOd() {
        return angazovanOd;
    }

    public void setAngazovanOd(Date angazovanOd) {
        this.angazovanOd = angazovanOd;
    }

    public Date getAngazovanDo() {
        return angazovanDo;
    }

    public void setAngazovanDo(Date angazovanDo) {
        this.angazovanDo = angazovanDo;
    }

    public String getBrojRacuna() {
        return brojRacuna;
    }

    public void setBrojRacuna(String brojRacuna) {
        this.brojRacuna = brojRacuna;
    }

    @XmlTransient
    public Collection<Predmet> getPredmetCollection() {
        return predmetCollection;
    }

    public void setPredmetCollection(Collection<Predmet> predmetCollection) {
        this.predmetCollection = predmetCollection;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Pozicija getPozicijaId() {
        return pozicijaId;
    }

    public void setPozicijaId(Pozicija pozicijaId) {
        this.pozicijaId = pozicijaId;
    }

    public Zvanje getZvanjeId() {
        return zvanjeId;
    }

    public void setZvanjeId(Zvanje zvanjeId) {
        this.zvanjeId = zvanjeId;
    }

    @XmlTransient
    public Collection<Plata> getPlataCollection() {
        return plataCollection;
    }

    public void setPlataCollection(Collection<Plata> plataCollection) {
        this.plataCollection = plataCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (korisnikId != null ? korisnikId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zaposlen)) {
            return false;
        }
        Zaposlen other = (Zaposlen) object;
        if ((this.korisnikId == null && other.korisnikId != null) || (this.korisnikId != null && !this.korisnikId.equals(other.korisnikId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ki401.entiteti.Zaposlen[ korisnikId=" + korisnikId + " ]";
    }
    
}
