/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.entiteti;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author taklap
 */
@Entity
@Table(name = "pozicije")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pozicija.findAll", query = "SELECT p FROM Pozicija p")
    , @NamedQuery(name = "Pozicija.findByPozicijaId", query = "SELECT p FROM Pozicija p WHERE p.pozicijaId = :pozicijaId")
    , @NamedQuery(name = "Pozicija.findByPozicijaNaziv", query = "SELECT p FROM Pozicija p WHERE p.pozicijaNaziv = :pozicijaNaziv")})
public class Pozicija implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pozicija_id")
    private Integer pozicijaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "pozicija_naziv")
    private String pozicijaNaziv;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pozicijaId")
    private Collection<Zaposlen> zaposlenCollection;

    public Pozicija() {
    }

    public Pozicija(Integer pozicijaId) {
        this.pozicijaId = pozicijaId;
    }

    public Pozicija(Integer pozicijaId, String pozicijaNaziv) {
        this.pozicijaId = pozicijaId;
        this.pozicijaNaziv = pozicijaNaziv;
    }

    public Integer getPozicijaId() {
        return pozicijaId;
    }

    public void setPozicijaId(Integer pozicijaId) {
        this.pozicijaId = pozicijaId;
    }

    public String getPozicijaNaziv() {
        return pozicijaNaziv;
    }

    public void setPozicijaNaziv(String pozicijaNaziv) {
        this.pozicijaNaziv = pozicijaNaziv;
    }

    @XmlTransient
    public Collection<Zaposlen> getZaposlenCollection() {
        return zaposlenCollection;
    }

    public void setZaposlenCollection(Collection<Zaposlen> zaposlenCollection) {
        this.zaposlenCollection = zaposlenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pozicijaId != null ? pozicijaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pozicija)) {
            return false;
        }
        Pozicija other = (Pozicija) object;
        if ((this.pozicijaId == null && other.pozicijaId != null) || (this.pozicijaId != null && !this.pozicijaId.equals(other.pozicijaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ki401.entiteti.Pozicija[ pozicijaId=" + pozicijaId + " ]";
    }
    
}
