/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.entiteti;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author taklap
 */
@Entity
@Table(name = "plata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plata.findAll", query = "SELECT p FROM Plata p")
    , @NamedQuery(name = "Plata.findByPlataId", query = "SELECT p FROM Plata p WHERE p.plataId = :plataId")
    , @NamedQuery(name = "Plata.findByIznos", query = "SELECT p FROM Plata p WHERE p.iznos = :iznos")
    , @NamedQuery(name = "Plata.findByZaMesec", query = "SELECT p FROM Plata p WHERE p.zaMesec = :zaMesec")})
public class Plata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "plata_id")
    private Integer plataId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "iznos")
    private long iznos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "za_mesec")
    private String zaMesec;
    @JoinColumn(name = "korisnik_id", referencedColumnName = "korisnik_id")
    @ManyToOne(optional = false)
    private Zaposlen korisnikId;

    public Plata() {
    }

    public Plata(Integer plataId) {
        this.plataId = plataId;
    }

    public Plata(Integer plataId, long iznos, String zaMesec) {
        this.plataId = plataId;
        this.iznos = iznos;
        this.zaMesec = zaMesec;
    }

    public Integer getPlataId() {
        return plataId;
    }

    public void setPlataId(Integer plataId) {
        this.plataId = plataId;
    }

    public long getIznos() {
        return iznos;
    }

    public void setIznos(long iznos) {
        this.iznos = iznos;
    }

    public String getZaMesec() {
        return zaMesec;
    }

    public void setZaMesec(String zaMesec) {
        this.zaMesec = zaMesec;
    }

    public Zaposlen getKorisnikId() {
        return korisnikId;
    }

    public void setKorisnikId(Zaposlen korisnikId) {
        this.korisnikId = korisnikId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plataId != null ? plataId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Plata)) {
            return false;
        }
        Plata other = (Plata) object;
        if ((this.plataId == null && other.plataId != null) || (this.plataId != null && !this.plataId.equals(other.plataId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ki401.entiteti.Plata[ plataId=" + plataId + " ]";
    }
    
}
