/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.ejb.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ki401.entiteti.Predmet;

/**
 *
 * @author taklap
 */
@Stateless
public class PredmetFacade extends AbstractFacade<Predmet> {

    @PersistenceContext(unitName = "Projekat_ki401PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PredmetFacade() {
        super(Predmet.class);
    }
    
}
