/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ki401.ejb.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ki401.entiteti.Zvanje;

/**
 *
 * @author taklap
 */
@Stateless
public class ZvanjeFacade extends AbstractFacade<Zvanje> {

    @PersistenceContext(unitName = "Projekat_ki401PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ZvanjeFacade() {
        super(Zvanje.class);
    }
    
}
