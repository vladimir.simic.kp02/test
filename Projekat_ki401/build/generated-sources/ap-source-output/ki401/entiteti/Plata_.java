package ki401.entiteti;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import ki401.entiteti.Zaposlen;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-07-09T13:39:27")
@StaticMetamodel(Plata.class)
public class Plata_ { 

    public static volatile SingularAttribute<Plata, Long> iznos;
    public static volatile SingularAttribute<Plata, String> zaMesec;
    public static volatile SingularAttribute<Plata, Zaposlen> korisnikId;
    public static volatile SingularAttribute<Plata, Integer> plataId;

}