package ki401.entiteti;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import ki401.entiteti.Rola;
import ki401.entiteti.Zaposlen;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-07-09T13:39:28")
@StaticMetamodel(Korisnik.class)
public class Korisnik_ { 

    public static volatile SingularAttribute<Korisnik, String> ime;
    public static volatile SingularAttribute<Korisnik, String> prezime;
    public static volatile SingularAttribute<Korisnik, String> password;
    public static volatile SingularAttribute<Korisnik, Date> datumRodjenja;
    public static volatile SingularAttribute<Korisnik, Boolean> aktivan;
    public static volatile SingularAttribute<Korisnik, Zaposlen> zaposlen;
    public static volatile SingularAttribute<Korisnik, Rola> rolaId;
    public static volatile SingularAttribute<Korisnik, Integer> korisnikId;
    public static volatile SingularAttribute<Korisnik, String> pol;
    public static volatile SingularAttribute<Korisnik, String> email;
    public static volatile SingularAttribute<Korisnik, String> username;

}