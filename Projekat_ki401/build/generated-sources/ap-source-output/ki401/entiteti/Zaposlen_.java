package ki401.entiteti;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import ki401.entiteti.Korisnik;
import ki401.entiteti.Plata;
import ki401.entiteti.Pozicija;
import ki401.entiteti.Predmet;
import ki401.entiteti.Zvanje;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-07-09T13:39:27")
@StaticMetamodel(Zaposlen.class)
public class Zaposlen_ { 

    public static volatile SingularAttribute<Zaposlen, Pozicija> pozicijaId;
    public static volatile SingularAttribute<Zaposlen, Date> angazovanOd;
    public static volatile CollectionAttribute<Zaposlen, Plata> plataCollection;
    public static volatile CollectionAttribute<Zaposlen, Predmet> predmetCollection;
    public static volatile SingularAttribute<Zaposlen, Zvanje> zvanjeId;
    public static volatile SingularAttribute<Zaposlen, Integer> korisnikId;
    public static volatile SingularAttribute<Zaposlen, Date> angazovanDo;
    public static volatile SingularAttribute<Zaposlen, String> brojRacuna;
    public static volatile SingularAttribute<Zaposlen, Korisnik> korisnik;

}