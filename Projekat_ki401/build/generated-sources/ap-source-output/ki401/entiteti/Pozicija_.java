package ki401.entiteti;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import ki401.entiteti.Zaposlen;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-07-09T13:39:27")
@StaticMetamodel(Pozicija.class)
public class Pozicija_ { 

    public static volatile SingularAttribute<Pozicija, Integer> pozicijaId;
    public static volatile SingularAttribute<Pozicija, String> pozicijaNaziv;
    public static volatile CollectionAttribute<Pozicija, Zaposlen> zaposlenCollection;

}