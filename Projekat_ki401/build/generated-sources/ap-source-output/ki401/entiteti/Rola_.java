package ki401.entiteti;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import ki401.entiteti.Korisnik;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-07-09T13:39:28")
@StaticMetamodel(Rola.class)
public class Rola_ { 

    public static volatile SingularAttribute<Rola, Integer> rolaId;
    public static volatile CollectionAttribute<Rola, Korisnik> korisnikCollection;
    public static volatile SingularAttribute<Rola, String> rolaNaziv;

}