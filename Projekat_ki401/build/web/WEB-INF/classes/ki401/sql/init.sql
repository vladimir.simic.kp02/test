INSERT INTO `zvanje` (`zvanje_id`, `zvanje_naziv`) 
VALUES 
   ('1', 'diplomirani inzenjer'),
   ('2', 'diplomirani ekonomista'),
   ('3', 'diplomirani pravnik');

INSERT INTO `pozicije` (`pozicija_id`, `pozicija_naziv`) 
VALUES 
   ('1', 'profesor'), 
   ('2', 'asistent');

INSERT INTO `predmeti` (`predmet_id`, `predmet_sifra`, `predmet_naziv`) 
VALUES 
   ('1', 'SRP', 'Srpski jezik i knjizevnost'),
   ('2', 'ENG', 'Engleski jezik');

INSERT INTO `role` (`rola_id`, `rola_naziv`) 
VALUES 
   ('1', 'Administrator'), 
   ('2', 'HR'), 
   ('3', 'Finan. sluzbenik'), 
   ('4', 'Zaposleni');

