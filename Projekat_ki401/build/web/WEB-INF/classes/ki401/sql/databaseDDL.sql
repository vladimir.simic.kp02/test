/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Veljko Grkovic
 * Created: Jun 28, 2018
 */
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bazaki401
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bazaki401` ;

-- -----------------------------------------------------
-- Schema bazaki401
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bazaki401` DEFAULT CHARACTER SET latin1 ;
USE `bazaki401` ;

-- -----------------------------------------------------
-- Table `role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `role` ;

CREATE TABLE IF NOT EXISTS `role` (
  `rola_id` INT(11) NOT NULL AUTO_INCREMENT,
  `rola_naziv` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`rola_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;

CREATE UNIQUE INDEX `rola_naziv_UNIQUE` ON `role` (`rola_naziv` ASC);


-- -----------------------------------------------------
-- Table `korisnik`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `korisnik` ;

CREATE TABLE IF NOT EXISTS `korisnik` (
  `korisnik_id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(20) NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  `email` VARCHAR(30) NOT NULL,
  `ime` VARCHAR(45) NOT NULL,
  `prezime` VARCHAR(45) NOT NULL,
  `datum_rodjenja` DATE NOT NULL,
  `pol` ENUM('M', 'Z') NOT NULL,
  `aktivan` TINYINT(1) NOT NULL DEFAULT '1',
  `rola_id` INT(11) NOT NULL,
  PRIMARY KEY (`korisnik_id`),
  CONSTRAINT `fk_korisnik_role`
    FOREIGN KEY (`rola_id`)
    REFERENCES `role` (`rola_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE UNIQUE INDEX `username_UNIQUE` ON `korisnik` (`username` ASC);

CREATE UNIQUE INDEX `email_UNIQUE` ON `korisnik` (`email` ASC);

CREATE INDEX `fk_korisnik_role_idx` ON `korisnik` (`rola_id` ASC);


-- -----------------------------------------------------
-- Table `pozicije`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pozicije` ;

CREATE TABLE IF NOT EXISTS `pozicije` (
  `pozicija_id` INT(11) NOT NULL AUTO_INCREMENT,
  `pozicija_naziv` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`pozicija_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE UNIQUE INDEX `pozicija_naziv_UNIQUE` ON `pozicije` (`pozicija_naziv` ASC);


-- -----------------------------------------------------
-- Table `zvanje`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zvanje` ;

CREATE TABLE IF NOT EXISTS `zvanje` (
  `zvanje_id` INT(11) NOT NULL AUTO_INCREMENT,
  `zvanje_naziv` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`zvanje_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE UNIQUE INDEX `zvanje_naziv_UNIQUE` ON `zvanje` (`zvanje_naziv` ASC);


-- -----------------------------------------------------
-- Table `zaposleni`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zaposleni` ;

CREATE TABLE IF NOT EXISTS `zaposleni` (
  `korisnik_id` INT(11) NOT NULL,
  `angazovan_od` DATE NOT NULL,
  `angazovan_do` DATE NULL DEFAULT NULL,
  `broj_racuna` VARCHAR(30) NOT NULL,
  `pozicija_id` INT(11) NOT NULL,
  `zvanje_id` INT(11) NOT NULL,
  PRIMARY KEY (`korisnik_id`),
  CONSTRAINT `fk_zaposleni_korisnik1`
    FOREIGN KEY (`korisnik_id`)
    REFERENCES `korisnik` (`korisnik_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_zaposleni_pozicije1`
    FOREIGN KEY (`pozicija_id`)
    REFERENCES `pozicije` (`pozicija_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zaposleni_zvanje1`
    FOREIGN KEY (`zvanje_id`)
    REFERENCES `zvanje` (`zvanje_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE UNIQUE INDEX `broj_racuna_UNIQUE` ON `zaposleni` (`broj_racuna` ASC);

CREATE INDEX `fk_zaposleni_pozicije1_idx` ON `zaposleni` (`pozicija_id` ASC);

CREATE INDEX `fk_zaposleni_zvanje1_idx` ON `zaposleni` (`zvanje_id` ASC);


-- -----------------------------------------------------
-- Table `plata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `plata` ;

CREATE TABLE IF NOT EXISTS `plata` (
  `plata_id` INT(11) NOT NULL AUTO_INCREMENT,
  `korisnik_id` INT(11) NOT NULL,
  `iznos` DECIMAL(10,0) NOT NULL,
  `za_mesec` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`plata_id`),
  CONSTRAINT `fk_plata_zaposleni1`
    FOREIGN KEY (`korisnik_id`)
    REFERENCES `zaposleni` (`korisnik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `fk_plata_zaposleni1_idx` ON `plata` (`korisnik_id` ASC);


-- -----------------------------------------------------
-- Table `predmeti`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `predmeti` ;

CREATE TABLE IF NOT EXISTS `predmeti` (
  `predmet_id` INT(11) NOT NULL AUTO_INCREMENT,
  `predmet_sifra` VARCHAR(6) NOT NULL,
  `predmet_naziv` VARCHAR(75) NOT NULL,
  PRIMARY KEY (`predmet_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE UNIQUE INDEX `predmet_sifra_UNIQUE` ON `predmeti` (`predmet_sifra` ASC);


-- -----------------------------------------------------
-- Table `zaposlenipredmeti`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zaposlenipredmeti` ;

CREATE TABLE IF NOT EXISTS `zaposlenipredmeti` (
  `korisnik_id` INT(11) NOT NULL,
  `predmet_id` INT(11) NOT NULL,
  PRIMARY KEY (`korisnik_id`, `predmet_id`),
  CONSTRAINT `fk_zaposleni_has_predmeti_predmeti1`
    FOREIGN KEY (`predmet_id`)
    REFERENCES `predmeti` (`predmet_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zaposleni_has_predmeti_zaposleni1`
    FOREIGN KEY (`korisnik_id`)
    REFERENCES `zaposleni` (`korisnik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `fk_zaposleni_has_predmeti_predmeti1_idx` ON `zaposlenipredmeti` (`predmet_id` ASC);

CREATE INDEX `fk_zaposleni_has_predmeti_zaposleni1_idx` ON `zaposlenipredmeti` (`korisnik_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


